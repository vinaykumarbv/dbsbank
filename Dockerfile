FROM openjdk:latest
Copy *.jar DBSBank-0.0.1-SNAPSHOT.jar
EXPOSE 8089
ENTRYPOINT ["java","-jar","DBSBank-0.0.1-SNAPSHOT.jar"]